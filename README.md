# Task 2 solution

### Part 1 test case

Navigate to `Careers` page and count available career categories

### Setup

Before running the tests, make sure that you have working NodeJS (at least v18.4.0) and npm (at least v8.12.1) installation. Using [NVM](https://github.com/nvm-sh/nvm) to install and manage NodeJS versions is recommended.

### Installing dependencies

To install project dependencies, inside repo run:

```shell
npm install
``` 

Then install Playwright browsers:
```shell
npx playwright install
```

### Running test

To run test in all browsers at the same time:

```shell
npm run all
```

To run in each browser individually:

```shell
npm run chromium
```
or 

```shell
npm run firefox
```

or

```shell
npm run webkit
```

Number of categories will be printed to console.

