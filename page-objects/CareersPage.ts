import { Locator, Page } from '@playwright/test';

export class CareersPage {
    private readonly categoriesList: Locator;

    constructor(private readonly page: Page) {
        this.categoriesList = this.page.locator('section.all-categories-list');
    }

    countCategories = async () => {
        await this.categoriesList.waitFor();
        const categories = await this.categoriesList.locator('div.col-lg-3 a');
        const count = await categories.count();
        console.log(`Careers page contains ${count} categories`);
    };
}
