import { Locator, Page } from '@playwright/test';

export class Navbar {
    private readonly navbarContainer: Locator;
    private readonly careersLink: Locator;

    constructor(private readonly page: Page) {
        this.navbarContainer = this.page.locator('div#navbarNav');
        this.careersLink = this.navbarContainer.locator('li', { hasText: 'Career' });
    }

    clickCareers = async () => {
        await this.careersLink.click();
    };
}
