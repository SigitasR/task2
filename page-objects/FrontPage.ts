import { Navbar } from './components/Navbar';
import { Page } from '@playwright/test';

export class FrontPage {
    readonly navBar: Navbar;

    constructor(private readonly page: Page) {
        this.navBar = new Navbar(page);
    }

    open = async () => {
        await this.page.goto('/');
    };
}
