import { test } from '@playwright/test';
import { FrontPage } from '../page-objects/FrontPage';
import { CareersPage } from '../page-objects/CareersPage';

test('should count career categories', async ({ page }) => {
    const frontPage = new FrontPage(page);
    const careersPage = new CareersPage(page);

    await frontPage.open();
    await frontPage.navBar.clickCareers();
    await careersPage.countCategories();
});
